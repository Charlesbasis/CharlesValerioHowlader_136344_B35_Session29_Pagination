<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Pagination</h2>
    <p>The .pagination class provides pagination links:</p>
    <ul class="pagination">

        <?php
        $pages = 20;

        for($i=1;$i<=$pages;$i++) {
            echo "<li><a href='#'>$i</a></li>";
        }
?>
    </ul>
</div>

</body>
</html>

