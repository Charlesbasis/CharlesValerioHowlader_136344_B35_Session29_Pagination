<?php
require_once("../../../vendor/autoload.php");

use App\SummaryOfOrganization\SummaryOfOrganization;

$objSummaryOfOrganization = new SummaryOfOrganization();

$objSummaryOfOrganization->setData($_GET);
$oneData = $objSummaryOfOrganization->view("obj");

echo "ID: ".$oneData->id."<br>";
echo "Company Name: ".$oneData->company_name."<br>";
echo "Summary: ".$oneData->summary."<br>";
?>